const fs = require("fs");
const { parse } = require('csv-parse');

let result = {};
let deliviersResult = []
fs.createReadStream('./data/matches.csv').pipe(parse({ columns: true, dslimiter: ',' }))
    .on('data', function (data) {
        let seasons = data.season
        if (!result[seasons]) {
            result[seasons] = [data.id]
        } else {
            result[seasons].push(data.id)
        }
    }).on('end', function () {
        fs.createReadStream('./data/deliveries.csv').pipe(parse({ columns: true, dslimiter: ',' }))
            .on('data', function (data) {
                deliviersResult.push(data)
            }).on('end', function () {
                // console.log(result)
                console.log(toGetOutput(result, deliviersResult));
            })
    })


function toGetOutput(result, deliviersResult) {
    let csvFile = deliviersResult.reduce(function (initialObject, objectComing) {
        for (let year in result) {
            for (let i = 0; i < result[year].length; i++) { // Iterating through matchIds
                if (result[year][i] === objectComing.match_id) { // Checking Id Matches with match_id
                    if (!initialObject[year]) {
                        initialObject[year] = {}
                    } else {
                        let checkingYearTeam = objectComing.bowling_team
                        if (!initialObject[year][checkingYearTeam]) {
                            initialObject[year][checkingYearTeam] = {};
                        } else {
                            let checkingYearTeamBowler = objectComing.bowler

                            if (!initialObject[year][checkingYearTeam][checkingYearTeamBowler] && parseInt(objectComing.extra_runs) !== 0) {
                                initialObject[year][checkingYearTeam][checkingYearTeamBowler] = parseInt(objectComing.extra_runs);
                            } else {
                                if (initialObject[year][checkingYearTeam][checkingYearTeamBowler]) {
                                    initialObject[year][checkingYearTeam][checkingYearTeamBowler] = initialObject[year][checkingYearTeam][checkingYearTeamBowler] + parseInt(objectComing.extra_runs)
                                }

                            }
                        }
                    }
                }
            }
        }
        return initialObject;
    }, {})
    let finalObject = {}
    for (let years in csvFile) {
        let objectYear = {}
        for (let team in csvFile[years]) {
            let objectToArray = Object.entries(csvFile[years][team]).sort((a, b) => b[1] - a[1]).slice(0, 3)
            let arrayToObject = Object.fromEntries(objectToArray)
            objectYear[team] = arrayToObject
        }
        finalObject[years]=objectYear
    }
    return finalObject;
}

